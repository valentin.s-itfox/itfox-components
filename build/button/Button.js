import { __assign, __extends } from "tslib";
import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Button.prototype.render = function () {
        var _a = this.props, buttonProps = _a.buttonProps, buttonStyle = _a.buttonStyle, textStyle = _a.textStyle, textProps = _a.textProps, title = _a.title;
        return (React.createElement(TouchableOpacity, __assign({ activeOpacity: 0.7, style: __assign(__assign({}, styles.button), buttonStyle) }, buttonProps),
            React.createElement(Text, __assign({ style: __assign(__assign({}, styles.text), textStyle) }, textProps), title)));
    };
    return Button;
}(React.Component));
export { Button };
var styles = StyleSheet.create({
    button: {
        maxWidth: '100%',
        width: '100%',
        borderRadius: 16,
        paddingVertical: 16,
        alignItems: 'center',
        backgroundColor: '#370BF0'
    },
    text: {
        fontFamily: 'lato',
        fontWeight: 'bold',
        fontSize: 14,
        lineHeight: 17,
        color: '#FFFFFF'
    }
});
export default Button;
//# sourceMappingURL=Button.js.map