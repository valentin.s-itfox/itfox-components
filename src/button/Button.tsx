import React from 'react'
import {TouchableOpacity, Text, StyleSheet, TouchableOpacityProps, ViewStyle, TextStyle, TextProps} from 'react-native'

interface Props {
  buttonProps?: TouchableOpacityProps;
  textProps?: TextProps;
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
  title: string
}

export class Button extends React.Component<Props> {

  render() {
    const {buttonProps, buttonStyle, textStyle, textProps, title} = this.props
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{...styles.button, ...buttonStyle}}
        {...buttonProps}
      >
        <Text style={{...styles.text, ...textStyle}} {...textProps}>{title}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    maxWidth: '100%',
    width: '100%',
    borderRadius: 16,
    paddingVertical: 16,
    alignItems: 'center',
    backgroundColor: '#370BF0'
  },
  text: {
    fontFamily: 'lato',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 17,
    color: '#FFFFFF'
  }
})

export default Button